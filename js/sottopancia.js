$(document).ready(function() {
    // toggle open/close div abbonati
    $('.sp-toggle, .sp-abbonati-title').on('click', function(e){
        e.preventDefault();
        scrollInit = window.scrollY;
        $('.sp-abbonati').toggleClass('open');
        $('.sp-toggle').toggleClass('toggle-open').text($('.sp-toggle').text() == 'espandi' ? 'chiudi' : 'espandi');

    });
    var uomoVogue = function(currentSlide) {
        if ( $(".sp-abbonati").attr('id') === "vogue-abbonati" ) {
            if (currentSlide.attr("id") === "slide4") {
                $('.sp-abbonati-title a').html('Abbonati e Regala L\'Uomo Vogue');
                $('.sp-copertina-big').attr('src', 'img/uomo-copertina-big.png');
                $('.sp-copertina').attr('src', 'img/uomo-copertina.png');
                $('.sp-button-large').addClass('uomo-button').html('Abbonati o regala l\'uomo Vogue');
            } else {
                $('.sp-copertina-big').attr('src', 'img/vogue-copertina-big.png');
                $('.sp-copertina').attr('src', 'img/vogue-copertina.png');
                $('.sp-abbonati-title a').html("Abbonati e Regala Vogue Italia! <span>edizione digitale inclusa</span>");
                $('.sp-button-large').removeClass('uomo-button').html('Abbonati o regala vogue');
            }
        } else {
            return false
        }
    }

        // carouFredSel carousel settings
    $('.sp-slider-content').carouFredSel({
        auto: {
            play: false
        },
        prev: {
            button: $('.sp-left'),
            key: 'left',
            onAfter: function() {
                uomoVogue($('.sp-slider-content').triggerHandler('currentVisible'));
            }
        },
        next: {
            button: $('.sp-right'),
            key: 'right',
            onAfter: function() {
                uomoVogue($('.sp-slider-content').triggerHandler('currentVisible'));
            }
        },
        pagination: {
            container: $('.sp-nav-items'),
            anchorBuilder: function(nr, item) {
                return '<a href="#'+nr+'"><span>&#9679;</span></a>';
            }
        }
    });
    $('.caroufredsel_wrapper').css({
        'z-index': 0
    });

    $(window).on('scroll', function(){
        if ( $('.sp-abbonati').hasClass('open') && ( window.scrollY > (scrollInit + 120) || window.scrollY < (scrollInit - 120) )) {
                $('.sp-abbonati').removeClass('open');
                $('.sp-toggle').removeClass('toggle-open').text('espandi');
        }
    })
});